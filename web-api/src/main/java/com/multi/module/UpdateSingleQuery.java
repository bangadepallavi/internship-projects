package com.multi.module;

import java.lang.*;
import java.sql.*;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UpdateSingleQuery {
    private static final Logger logger = LogManager.getLogger(UpdateSingleQuery.class);
    private Scanner scanner= new Scanner(System.in);
    public  void fetchData(Connection connection)
    {
        PreparedStatement preparedStatement= null;
        ResultSet resultSet=null;
        try {
            logger.info("Enter the department name to be update all rows:");
            String updateDeptName = scanner.next();
             preparedStatement = connection.prepareStatement("update users set dept_name=? returning id,name,age,dept_name");
            preparedStatement.setString(1, updateDeptName);
            preparedStatement.execute();
            logger.info("After update all rows");
             resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                logger.info(resultSet.getInt("id") + " " + resultSet.getString("name") + " " + resultSet.getInt("age") + " " + resultSet.getString("dept_name"));
            }
        }
        catch (SQLException sqlException)
        {
            logger.error(sqlException);
        }
        catch (Exception exception)
        {
            logger.error(exception);
        }
        finally {
            if (resultSet!=null)
            {
                try {
                    resultSet.close();
                }
                catch (SQLException sqlException)
                {
                    logger.error(sqlException);
                }
            }
        }
    }
    public static void main(String args[])throws  SQLException
    {
        Connection connection=null;
        try {
            String url = "jdbc:postgresql://localhost:5432/internship_samples";
            String user = "postgres";
            String password = "postgres";
             connection = DriverManager.getConnection(url, user, password);
            UpdateSingleQuery usq = new UpdateSingleQuery();
            usq.fetchData(connection);
        }
        catch (SQLException sqlException)
        {
            logger.error(sqlException);
        }
        catch (Exception exception)
        {
            logger.error(exception);
        }
        finally {
            if (connection!=null)
            {
                try
                {
                    connection.close();
                }
                catch (Exception exception)
                {

                }
            }
        }
    }
}
