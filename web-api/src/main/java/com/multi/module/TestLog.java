package com.multi.module;

import  org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestLog {
    public static Logger log=LogManager.getLogger(TestLog.class);
    public  static  void main(String args[])
    {
        String param="abc user";
        log.info("Information");
        log.trace("Trace message");
        log.debug("Debugging");
        log.error("Error occured");
        log.fatal("Fatal error occured by {}",param);
    }

}
