package com.multi.module;

import java.lang.*;
import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataInputOutput
{
        private static final Logger logger = LogManager.getLogger(DataInputOutput.class);
        private Scanner scanner=new Scanner(System.in);
        public void insertTenRecord(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            try {
                 preparedStatement = connection.prepareStatement("INSERT INTO users VALUES(?,?,?,?) ");
                for (int n = 1; n <= 10; n++) {
                    try {
                        logger.info("Enter details for user:{}", n);
                        logger.info("Enter user id");
                         int userId = scanner.nextInt();
                        logger.info("Enter user name");
                         String userName = scanner.next();
                        logger.info("Enter user age");
                        int userAge = scanner.nextInt();
                        logger.info("Enter user department name");
                        String userDepartment = scanner.next();
                        preparedStatement.setInt(1, userId);
                        preparedStatement.setString(2, userName);
                        preparedStatement.setInt(3, userAge);
                        preparedStatement.setString(4, userDepartment);
                        preparedStatement.executeUpdate();
                        logger.info("Successfully inserted.");

                    }
                    catch (InputMismatchException ime) {
                        logger.error("Can not insert Invalid data");
                        break;
                    }
                }
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException sqlException) {
                        logger.error(sqlException);
                    }
                }

            }
        }

        public void selectAllRecords(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                 preparedStatement = connection.prepareStatement("SELECT * FROM users");
                 resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        logger.info("{} {} {} {}",resultSet.getInt("id") , resultSet.getString("name"), resultSet.getInt("age"),resultSet.getString("dept_name"));
                    }
                    logger.info("all rows are selected...");
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void selectFirstRecord(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                 preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
                 resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    logger.info("{} {} {} {}",resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("age"),resultSet.getString("dept_name"));
                    logger.info("First row is selected...");
                }
                else
                {
                    logger.error("No data found.");
                }
            }
            catch (SQLException sqlException)
            {
                logger.error(sqlException);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void updateAllRecord(Connection connection){
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                logger.info("Enter the department name to be update all rows:");
                String userDepartment = scanner.next();
                 preparedStatement = connection.prepareStatement("update users set dept_name=?");
                preparedStatement.setString(1, userDepartment);
               int i= preparedStatement.executeUpdate();
               if(i>0)
               {
                   logger.info("Successfully updated all rows...");
               }
               else
               {
                   logger.error("Unable to update");
               }
                preparedStatement.close();
                logger.info("After update all rows");
                preparedStatement = connection.prepareStatement("SELECT * FROM users");
                 resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    logger.info("{} {} {} {}",resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("age"),resultSet.getString("dept_name"));
                }
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception) {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void updateFirstRecord(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                 preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
                 resultSet = preparedStatement.executeQuery();
                int updateId=0;
                if (resultSet.next()) {
                       updateId = resultSet.getInt("id");
                }
                resultSet.close();
                preparedStatement.close();
                logger.info("Enter the department name to be update for First row :");
                String updateDepartment = scanner.next();
                preparedStatement = connection.prepareStatement("UPDATE users set dept_name=? WHERE id=?");
                preparedStatement.setString(1, updateDepartment);
                preparedStatement.setInt(2, updateId);
                int i= preparedStatement.executeUpdate();
                if(i>0)
                {
                   logger.info("Successfully update first row...");
                }else
                {
                   logger.error("Unable to update first row .");
                }
                preparedStatement.close();
                logger.info("After update first");
                preparedStatement = connection.prepareStatement("SELECT * FROM users");
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    logger.info("{} {} {} {}",resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("age"),resultSet.getString("dept_name"));
                }
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void deleteFirstRecord(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                int deleteId;
                 preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
                 resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    deleteId = resultSet.getInt("id");
                    logger.info("before delete the first row is ");
                    logger.info("{} {} {} {}",resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("age"),resultSet.getString("dept_name"));
                    resultSet.close();
                    preparedStatement.close();
                    try
                    {
                        preparedStatement = connection.prepareStatement("delete from users WHERE id=?");
                        preparedStatement.setInt(1, deleteId);
                        int i = preparedStatement.executeUpdate();
                        if (i > 0)
                        {
                            logger.info("Successfully deleted First row");
                            logger.info("after delete the first row is ");
                            preparedStatement.close();
                            preparedStatement = connection.prepareStatement("SELECT * FROM users");
                            resultSet = preparedStatement.executeQuery();
                            while (resultSet.next())
                            {
                                logger.info("{} {} {} {}", resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("age"), resultSet.getString("dept_name"));
                            }
                        } else {
                            logger.error("Unable to update first row .");
                        }
                    }
                    catch (SQLException sqlException)
                    {
                        logger.error(sqlException);
                    }
                }
                else {
                    logger.error("No data found.");
                }
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void deleteAllRecord(Connection connection)
        {
            PreparedStatement preparedStatement=null;
            ResultSet resultSet=null;
            try {
                 preparedStatement = connection.prepareStatement("SELECT * from users");
                 resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    try {
                        preparedStatement.close();
                        preparedStatement = connection.prepareStatement("DELETE from users");
                        preparedStatement.executeUpdate();
                        logger.info("Successfully deleted all row");
                    }
                    catch (SQLException sqlException)
                    {
                        logger.error(sqlException);
                    }
                } else
                    logger.error("Unable to delete");
            }
            catch (SQLException sqlexception)
            {
                logger.error(sqlexception);
            }
            catch (Exception exception)
            {
                logger.error(exception);
            }
            finally {
                closeResource(resultSet,preparedStatement);
            }
        }

        public void closeResource(ResultSet resultSet,PreparedStatement preparedStatement)
        {
                if (resultSet != null)
                    try {
                        resultSet.close();
                    }
                    catch (SQLException sqlException) {
                        logger.error(sqlException);
                    }
                if (preparedStatement!=null)
                    try {
                    preparedStatement.close();
                     }
                    catch (SQLException sqlException)
                    {
                    logger.error(sqlException);
                    }
        }

    public static void main(String args[]){
        Scanner scanner=new Scanner(System.in);
        DataInputOutput  dataInputOutput= new DataInputOutput();
        try {
            String answer;
            String url="jdbc:postgresql://localhost:5432/internship_samples";
            String user="postgres";
            String password="postgres";
            Connection connection = DriverManager.getConnection(url, user, password);
            do {
                logger.info("Select one option :");
                logger.info("1) Insert 10 record");
                logger.info("2) Select all records");
                logger.info("3) Select first record");
                logger.info("4) Update all records");
                logger.info("5) Update first record");
                logger.info("6) Delete first record");
                logger.info("7)Delete all records");
               int choice = scanner.nextInt();
                switch (choice) {

                    case 1:
                        dataInputOutput.insertTenRecord(connection);
                        break;

                    case 2:
                        dataInputOutput.selectAllRecords(connection);
                        break;

                    case 3:
                        dataInputOutput.selectFirstRecord(connection);
                        break;

                    case 4:
                        dataInputOutput.updateAllRecord(connection);
                        break;

                    case 5:
                        dataInputOutput.updateFirstRecord(connection);
                        break;

                    case 6:
                        dataInputOutput.deleteFirstRecord(connection);
                        break;

                    case 7:
                        dataInputOutput.deleteAllRecord(connection);
                        break;
                    default:
                        logger.info("Please enter the proper choice");
                }
                logger.info("Do you want to continue press y to continue or n  to terminate?");
                answer = scanner.next();
            } while (answer.equals("y") || answer.equals("Y"));
            logger.error("Process is terminated");
            connection.close();
        }
        catch (InputMismatchException inputMismatchException)
        {
            logger.error("You have entered wrong choice");
        }
        catch (NumberFormatException numberFormatException)
        {
            logger.error("Enter the proper option");
        }
        catch (NullPointerException nullPointerException)
        {
            logger.error(nullPointerException);
        }
        catch (SQLException sqlexception)
        {
            logger.error(sqlexception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
        }
        finally
        {
            scanner.close();
        }
    }
}
