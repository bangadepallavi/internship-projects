package com.connection.pooling;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import  com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;
public class TestConnectionPooling {

   private static final Logger logger = LogManager.getLogger(TestConnectionPooling.class);
   Scanner scanner=new Scanner(System.in);
     public  void fetchData(Connection connection)
    {

        try(PreparedStatement preparedStatement = connection.prepareStatement("update users set dept_name=? returning id,name,age,dept_name")) {
            logger.info("Enter the department name to be update all rows : ");
            String updateDeptName = scanner.next();
            preparedStatement.setString(1, updateDeptName);
            preparedStatement.execute();
            try( ResultSet resultSet = preparedStatement.getResultSet())
            {
                logger.info("After update all rows .");
                while (resultSet.next()) {
                    logger.info(resultSet.getInt("id") + " " + resultSet.getString("name") + " " + resultSet.getInt("age") + " " + resultSet.getString("dept_name"));
                }
            }
            catch (SQLException sqlException)
            {
                logger.error(sqlException);
            }

        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }

    }

    public static void main(String[] args)
    {
        HikariConfig conf=new HikariConfig();
        conf.setJdbcUrl("jdbc:postgresql://localhost:5432/internship_samples");
        conf.setUsername("postgres");
        conf.setPassword("postgres");
        conf.setAutoCommit(false);
        conf.setMaximumPoolSize(10);
        conf.setConnectionTimeout(2000);
        DataSource dataSource=new HikariDataSource(conf);
        try(Connection connection = dataSource.getConnection())
        {
            logger.info("The maximum time to get connection is :{} seconds",dataSource.getLoginTimeout());
            TestConnectionPooling t=new TestConnectionPooling();
            t.fetchData(connection);
            connection.commit();
        }
        catch(SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception e)
        {
            logger.error(e);
        }
    }

}
