package com.connection.pooling;
import java.sql.*;
import javax.sql.DataSource;
import  com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;
public class DataInputOutputConnectionPooling {
       public  static final Logger logger=LogManager.getLogger(DataInputOutputConnectionPooling.class);
    private Scanner scanner=new Scanner(System.in);
    public void insertTenRecords(Connection connection)
    {
        try(PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users VALUES(?,?,?,?) ");)
        {
            for (int n = 0b00_01; n <= 0b10_10; n++) {
                try {
                    logger.info("Enter details for user: {}", n);
                    logger.info("Enter user id :");
                    int userId = scanner.nextInt();
                    logger.info("Enter user name :");
                    String userName = scanner.next();
                    logger.info("Enter user age :");
                    int userAge = scanner.nextInt();
                    logger.info("Enter user department name :");
                    String userDepartment = scanner.next();
                    preparedStatement.setInt(1, userId);
                    preparedStatement.setString(2, userName);
                    preparedStatement.setInt(3, userAge);
                    preparedStatement.setString(4, userDepartment);
                    preparedStatement.executeUpdate();
                    logger.info("Successfully inserted .");

                }
                catch (InputMismatchException |NumberFormatException exception) {
                    logger.error("Can not insert Invalid data.");
                    break;
                }
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
    }

    public void selectAllRecords(Connection connection)
    {
        try(PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users");ResultSet resultSet = preparedStatement.executeQuery();)
        {
            while (resultSet.next()) {
                logger.info("{} {} {} {}",resultSet.getInt("id") , resultSet.getString("name"), resultSet.getInt("age"),resultSet.getString("dept_name"));
            }
            logger.info("all rows are selected..");
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
    }

    public void selectFirstRecord(Connection connection)
    {
        try( PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
             ResultSet resultSet = preparedStatement.executeQuery())
        {

            if (resultSet.next()) {
                logger.info("{} {} {} {}",resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("age"),resultSet.getString("dept_name"));
                logger.info("First row is selected...");
            }
            else
            {
                logger.error("No data found .");
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
    }

    public void updateAllRecord(Connection connection){
        try(PreparedStatement preparedStatement = connection.prepareStatement("update users set dept_name=?")) {
            logger.info("Enter the department name to be update all rows : ");
            String userDepartment = scanner.next();
            preparedStatement.setString(1, userDepartment);
            int i= preparedStatement.executeUpdate();
            if(i>0B0000)
            {
                logger.info("Successfully updated all rows...");
            }
            else
            {
                logger.error("Unable to update");
            }
            logger.info("After update all rows");
            try(PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM users");
                ResultSet resultSet = preparedstatement.executeQuery())
            {
                while (resultSet.next())
                {
                    logger.info("{} {} {} {}", resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("age"), resultSet.getString("dept_name"));
                }
            }
            catch(SQLException exception)
            {
                logger.error(exception);
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }
    }

    public void updateFirstRecord(Connection connection)
    {
        int updateId=0b00_00;
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
             ResultSet resultSet = preparedStatement.executeQuery())
        {

            if (resultSet.next())
            {
                updateId = resultSet.getInt("id");
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        logger.info("Enter the department name to be update  for First row:");
        String updateDepartment = scanner.next();
        try(PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users set dept_name=? WHERE id=?"))
        {
            preparedStatement.setString(1, updateDepartment);
            preparedStatement.setInt(2, updateId);
            int i = preparedStatement.executeUpdate();
            if (i > 0b00_00) {
                logger.info("Successfully update first row....");
            } else {
                logger.error("Unable to update first row.");
            }
        }
        catch (SQLException exception)
        {
            logger.error(exception);
        }
        logger.info("After update first");
        try(PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM users");
            ResultSet resultset = preparedstatement.executeQuery())
        {
            while (resultset.next()) {
                logger.info("{} {} {} {}", resultset.getInt("id"), resultset.getString("name"), resultset.getInt("age"), resultset.getString("dept_name"));
            }
        }
        catch(SQLException exception)
        {
            logger.error(exception);
        }
    }

    public void deleteFirstRecord(Connection connection)
    {
        int deleteId;
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users limit 1");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next())
            {
                deleteId = resultSet.getInt("id");
                logger.info("before delete the first row is :");
                logger.info("{} {} {} {}", resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("age"), resultSet.getString("dept_name"));
                try (PreparedStatement preparedstatement = connection.prepareStatement("delete from users WHERE id=?"))
                {
                    preparedstatement.setInt(1, deleteId);
                    int i = preparedstatement.executeUpdate();
                    if (i > 0b00_00)
                    {
                        logger.info("Successfully deleted First row..");
                    }
                    else
                    {
                        logger.error("Unable to delete first row .");
                    }
                } catch (SQLException sqlException) {
                    logger.error(sqlException);
                }
                logger.info("after delete the first row is .");
                try(PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM users");
                    ResultSet resultset = preparedstatement.executeQuery())
                {
                    while (resultset.next())
                    {
                        logger.info("{} {} {} {}", resultset.getInt("id"), resultset.getString("name"), resultset.getInt("age"), resultset.getString("dept_name"));
                    }
                }
                catch(SQLException exception)
                {
                    logger.error(exception);
                }
            }
            else
            {
                logger.error("No data found.");
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
    }

    public void deleteAllRecord(Connection connection)
    {
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users");
             ResultSet resultSet = preparedStatement.executeQuery())
        {
            if (resultSet.next()) {
                try (PreparedStatement preparedstatement = connection.prepareStatement("DELETE from users"))
                {
                    preparedstatement.executeUpdate();
                    logger.info("Successfully deleted all row...");
                }
                catch (SQLException sqlException)
                {
                    logger.error(sqlException);
                    throw sqlException;
                }
            } else
                logger.error("Unable to delete");
        }
        catch (NullPointerException | SQLException exception)
        {
            logger.error(exception);
        }
    }
    public static void main(String args[]){

        HikariConfig conf=new HikariConfig();
        conf.setPoolName("connectionPooling");
        conf.setJdbcUrl("jdbc:postgresql://localhost:5432/internship_samples");
        conf.setUsername("postgres");
        conf.setPassword("postgres");
        conf.setAutoCommit(false);
        conf.setMaximumPoolSize(10);
        conf.setConnectionTimeout(3000);
        String answer;
        DataSource dataSource=new HikariDataSource(conf);

        try (Scanner scanner=new Scanner(System.in);Connection connection = dataSource.getConnection())
        {
            logger.info("The time of data source to get connection is :{} seconds",dataSource.getLoginTimeout());
            DataInputOutputConnectionPooling dataInputOutputConnectionPooling=new DataInputOutputConnectionPooling();
            do {
                logger.info("Enter the choice in word (ex:Enter 'one' for Insert 10 records'):");
                logger.info("1) Insert 10 record");
                logger.info("2) Select all records");
                logger.info("3) Select first record");
                logger.info("4) Update all records");
                logger.info("5) Update first record");
                logger.info("6) Delete first record");
                logger.info("7)Delete all records");
                String choice = scanner.next();
                switch (choice) {

                    case "one":
                        dataInputOutputConnectionPooling.insertTenRecords(connection);
                        break;

                    case "two":
                        dataInputOutputConnectionPooling.selectAllRecords(connection);
                        break;

                    case "three":
                        dataInputOutputConnectionPooling.selectFirstRecord(connection);
                        break;

                    case "four":
                        dataInputOutputConnectionPooling.updateAllRecord(connection);
                        break;

                    case "five":
                        dataInputOutputConnectionPooling.updateFirstRecord(connection);
                        break;

                    case "six":
                        dataInputOutputConnectionPooling.deleteFirstRecord(connection);
                        break;

                    case "seven":
                        dataInputOutputConnectionPooling.deleteAllRecord(connection);
                        break;
                    default:
                        logger.info("Please enter the proper choice in word(ex :'one') ");
                }
                logger.info("Do you want to continue press y to continue or n  to terminate?");
                answer = scanner.next();
            } while (answer.equals("y") || answer.equals("Y"));
            connection.commit();

            logger.error("Process is terminated");
        }
        catch (InputMismatchException |NumberFormatException|NullPointerException|SQLException  multipleException)
        {
            logger.error("You have entered wrong choice :{}",multipleException.getMessage());
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }

    }
}
