package com.java.seven;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;
public class DemoJavaSeven
{
public static final Logger logger=LogManager.getLogger(DemoJavaSeven.class);
    public static void main(String args[])
    {
        int firstNumber = 0b1000;
        int secondNumber = 0b01_10;
        logger.info("First Number ={}", firstNumber);
        logger.info("Second Number={}", secondNumber);
        logger.info("1) Addition");
        logger.info("2) Subtraction");
        logger.info("3) Multiplication");
        logger.info("4) Division");
        try (Scanner scanner = new Scanner(System.in))
        {
            logger.info("Enter your choice in word (ex:for addition enter 'one'):");
            String choice = scanner.next();
            switch (choice) {
                case "one":
                    logger.info("Addition={}", firstNumber + secondNumber);
                    break;

                case "two":
                    logger.info("Subtraction={}", firstNumber - secondNumber);
                    break;
                case "three":
                    logger.info("Multiplication={}", firstNumber * secondNumber);
                    break;
                case "four":
                    logger.info("Division={}", firstNumber / secondNumber);
                    break;
                default:
                    logger.error("Please enter proper choice .");
            }
        }
        catch (ArithmeticException | InputMismatchException exception)
        {
            logger.error(exception.getMessage());
            throw exception;
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }

        logger.info("Demo of generic class");
        TestGenericClass<Integer> integerTestGenericClass = new TestGenericClass<>();
        TestGenericClass<Float> floatTestGenericClass = new TestGenericClass<>();
        TestGenericClass<String> stringTestGenericClass = new TestGenericClass<>();

        integerTestGenericClass.acceptData(10);
        floatTestGenericClass.acceptData(new Float(50.2));
        stringTestGenericClass.acceptData("Hello World");

        logger.info("Integer Value : {}", integerTestGenericClass.displayData());
        logger.info("Float value : {}", floatTestGenericClass.displayData());
        logger.info("String Value : {}", stringTestGenericClass.displayData());

    }
}
class TestGenericClass<TestGeneric> {
    private TestGeneric testGeneric;

    @SuppressWarnings({"unchecked","varargs"})
    public void acceptData(TestGeneric testGeneric) {
        this.testGeneric = testGeneric;
    }

    @SuppressWarnings({"unchecked","varargs"})
    public TestGeneric displayData() {
        return testGeneric;

    }
    }

