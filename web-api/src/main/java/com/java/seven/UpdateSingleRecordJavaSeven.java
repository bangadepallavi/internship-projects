package com.java.seven;
import java.lang.*;
import java.sql.*;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class UpdateSingleRecordJavaSeven {
    public static final Logger logger=LogManager.getLogger(UpdateSingleRecordJavaSeven.class);
    private Scanner scanner= new Scanner(System.in);
    public  void fetchData(Connection connection)
    {
        try(PreparedStatement preparedStatement = connection.prepareStatement("update users set dept_name=? returning id,name,age,dept_name");
            ResultSet resultSet = preparedStatement.getResultSet()) {
            logger.info("Enter the department name to be update all rows :");
            String updateDeptName = scanner.next();
            preparedStatement.setString(1, updateDeptName);
            preparedStatement.execute();
            logger.info("After update all rows");
            while (resultSet.next()) {
                logger.info(resultSet.getInt("id") + " " + resultSet.getString("name") + " " + resultSet.getInt("age") + " " + resultSet.getString("dept_name"));
            }
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }

    }

    public static void main(String args[])
    {
        String url = "jdbc:postgresql://localhost:5432/internship_samples";
        String user = "postgres";
        String password = "postgres";
        try (Connection connection = DriverManager.getConnection(url, user, password))
        {
            UpdateSingleRecordJavaSeven usq = new UpdateSingleRecordJavaSeven();
            usq.fetchData(connection);
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }
        }
}
