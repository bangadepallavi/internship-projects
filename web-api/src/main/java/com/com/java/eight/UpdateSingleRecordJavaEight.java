package com.com.java.eight;
import java.lang.*;
import java.sql.*;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.DriverAction;

public class UpdateSingleRecordJavaEight implements DriverAction{
    public static final Logger logger=LogManager.getLogger(UpdateSingleRecordJavaEight.class);
    private Scanner scanner= new Scanner(System.in);
    public void deregister()
    {
        logger.info("Driver Deregister");
    }
    public  void fetchData(Connection connection)
    {
        try(PreparedStatement preparedStatement = connection.prepareStatement("update users set dept_name=? returning id,name,age,dept_name")) {
            logger.info("Enter the department name to be update all rows :");
            String updateDeptName = scanner.next();
            preparedStatement.setString(1, updateDeptName);
            preparedStatement.execute();
           try( ResultSet resultSet = preparedStatement.getResultSet())
           {
               logger.info("After update all rows");
               while (resultSet.next()) {
                   logger.info(resultSet.getInt("id") + " " + resultSet.getString("name") + " " + resultSet.getInt("age") + " " + resultSet.getString("dept_name"));
               }
           }
           catch (SQLException sqlException)
           {
               logger.error(sqlException);
           }

        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }

    }

    public static void main(String args[])
    {
        Driver driver= new org.postgresql.Driver();
        DriverAction driverAction=new UpdateSingleRecordJavaEight();
        try {
            DriverManager.registerDriver(driver, driverAction);
        }
        catch (SQLException exception)
        {
            logger.error(exception);
        }
        String url = "jdbc:postgresql://localhost:5432/internship_samples";
        String user = "postgres";
        String password = "postgres";
        try (Connection connection = DriverManager.getConnection(url, user, password))
        {
            UpdateSingleRecordJavaEight usq = new UpdateSingleRecordJavaEight();
            usq.fetchData(connection);
            DriverManager.deregisterDriver(driver);
        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
            throw exception;
        }
    }
}
