package com.com.java.eight;

import java.sql.DriverAction;
import java.time.LocalDate;
import java.util.*;
import java.sql.*;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DemoJavaEight implements DriverAction{
    public static final Logger logger=LogManager.getLogger(DemoJavaEight.class);
    public void deregister() {
        logger.info("Driver deregistered");
    }

    public static void main(String args[])
    {
        logger.info("----Lambda Expression-----");
        CheckNumber evenNumber=n->{
            if(n%2==0)
                return "Even";
            else
                return "odd";
        };
        logger.info(evenNumber.checkNumber(5));

        logger.info("----Collection---");
        List<String> insertElement = new ArrayList<>();
        insertElement.add("Ram");
        insertElement.add("Ashwini");
        insertElement.add("Shreya");
        insertElement.add("Aniket");
        insertElement.add("Ayansh");
        insertElement.add("Harshal");
        insertElement.add("Aniket");
        logger.info("Size of array is :"+insertElement.size());
        insertElement.sort(Comparator.naturalOrder());
        int i=1;
        for (String s:insertElement) {
            logger.info("{}) {}",i,s);
            i++;
        }
        List<String> list=insertElement.stream().distinct().collect(Collectors.toList());
        logger.info("Distinct names are : {}",list);

        logger.info("---Date and time---");
        LocalDate today=LocalDate.now();
        int date=today.getDayOfMonth();
        logger.info("Today's date is :{}",today);
        logger.info("Today's day is :{}",today.getDayOfWeek());
        logger.info("Date after two days from today :{}",today.plusDays(2));
        LocalDate birthday=LocalDate.of(1995,7,18);
        logger.info("Date of birth is :{} and day is :{}",birthday,birthday.getDayOfWeek());
        if(today.getMonth().equals(birthday.getMonth())&& date==birthday.getDayOfMonth())
        {
            logger.info("Wish you a very happy birthday ...!");
        }
        else
        {
            logger.info("What is your date of birth ?");
        }
        logger.info("---java.lang and java.util Packages---");
        Base64.Encoder enc=Base64.getEncoder();
        Base64.Decoder dec=Base64.getDecoder();
        String message="Message to encode";
        logger.info("Original message is :"+message);
        try {
            byte[] encodeMessage=enc.encode(message.getBytes("UTF8"));
            logger.info("Encoded message is:{}",new String(encodeMessage,"UTF8"));
            byte[] decodeMessage=dec.decode(encodeMessage);
            logger.info("Decoded message is:"+new String(decodeMessage,"UTF8"));
        }
        catch (Exception e)
        {
            logger.info(e);
        }
        String s="8989100011";
        long n=Long.parseUnsignedLong(s);
        logger.info("String value is converted to Long : {}",n);

        logger.info("-----JDBC-----");
        try{
            Driver driver = new org.postgresql.Driver();
            DriverAction driverAction = new DemoJavaEight();
            DriverManager.registerDriver(driver, driverAction);
            try(Connection connection=DriverManager.getConnection("jdbc:postgresql://localhost:5432/internship_samples","postgres","postgres")) {
                try (Statement statement = connection.createStatement();
                     ResultSet resultSet = statement.executeQuery("select * from users")) {
                    while (resultSet.next()) {
                        logger.info("{} {} {} {}", resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
                    }
                } catch (SQLException exception) {
                    logger.error(exception);
                }
            }
            catch (SQLException exception)
            {
                logger.error(exception);
            }
            DriverManager.deregisterDriver(driver);
        }catch(Exception e){
            logger.error(e);
        }
    }
}
interface CheckNumber
{
    String checkNumber(int n);
}
