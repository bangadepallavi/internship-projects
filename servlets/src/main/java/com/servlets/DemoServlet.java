package com.servlets;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@WebServlet(name = "hello",urlPatterns = {"/api/hello-world"},displayName = "Hello UserName")
public class DemoServlet extends HttpServlet{
    public static final Logger logger=LogManager.getLogger(DemoServlet.class);
    @Override
    public void doGet(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
        HttpSession session = httpServletRequest.getSession(true);
        Date createTime = new Date(session.getCreationTime());
        Date lastAccessTime = new Date(session.getLastAccessedTime());
        String title = "Welcome  to my website (Get method)";
        Integer visitCount = new Integer(0);
        String visitCountKey = new String("visitCount");
        try {
            if (session.isNew()) {
                title = "Welcome to my web page (Get method)";

            } else {
                visitCount = (Integer) session.getAttribute(visitCountKey);
                visitCount = visitCount + 1;
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }
        try {
            httpServletResponse.setContentType("text/html");
            if(!httpServletResponse.isCommitted()) {
                String userName = httpServletRequest.getParameter("getUserName");
                session.setAttribute("title", title);
                session.setAttribute("createTime", createTime);
                session.setAttribute("lastAccessTime", lastAccessTime);
                session.setAttribute("userId", userName);
                session.setAttribute("visitCount", visitCount);
                session.setMaxInactiveInterval(2);
                if (!userName.isEmpty()) {
                    httpServletRequest.getRequestDispatcher("/web/HelloUserName.jsp?message=hello," + userName).forward(httpServletRequest, httpServletResponse);
                } else {
                    httpServletRequest.getRequestDispatcher("/web/HelloUserName.jsp?message=please enter user name").forward(httpServletRequest, httpServletResponse);
                }
            }
        }
        catch (ServletException | IOException exception)
        {
            logger.error(exception);
        }
        catch (Exception exception)
        {
            logger.error(exception);
        }
    }

    @Override
public void doPost(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse)
{

       HttpSession session = httpServletRequest.getSession(true);

       Date createTime = new Date(session.getCreationTime());
       Date lastAccessTime = new Date(session.getLastAccessedTime());
       String title = "Welcome  to my website(post method)";
       Integer visitCount = new Integer(0);
       String visitCountKey = new String("visitCount");
       try {
           if (session.isNew()) {
               title = "Welcome to my web page (post method)";

           } else {
               visitCount = (Integer) session.getAttribute(visitCountKey);
               visitCount = visitCount + 1;
           }
       } catch (Exception e) {
           logger.error(e);
       }
       try {
           httpServletResponse.setContentType("text/html");
           String userName = httpServletRequest.getParameter("postUserName");

           if (!httpServletResponse.isCommitted()) {
               session.setAttribute("title", title);
               session.setAttribute("createTime", createTime);
               session.setAttribute("lastAccessTime", lastAccessTime);
               session.setAttribute("userId", userName);
               session.setAttribute("visitCount", visitCount);
               session.setMaxInactiveInterval(2);
               if (!userName.isEmpty()) {
                   httpServletRequest.getRequestDispatcher("/web/HelloUserName.jsp?message=hello, " + userName).forward(httpServletRequest, httpServletResponse);
               } else {
                   httpServletRequest.getRequestDispatcher("/web/HelloUserName.jsp?message=please enter user name").forward(httpServletRequest, httpServletResponse);
               }
           }
       } catch (ServletException | IOException exception) {
           logger.error(exception);
       } catch (Exception exception) {
           logger.error(exception);
       }

}
}
